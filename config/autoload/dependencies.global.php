<?php

use Worldstores\ContactBook\Api\Action;
use Worldstores\ContactBook\Api\Factory;
use Zend\Expressive\Application;
use Zend\Expressive\Container\ApplicationFactory;
use Zend\Expressive\Helper;

return [
    'dependencies' => [
        'invokables' => [
            Zend\Expressive\Router\RouterInterface::class => Zend\Expressive\Router\FastRouteRouter::class,
            Helper\ServerUrlHelper::class => Helper\ServerUrlHelper::class,
            GuzzleHttp\ClientInterface::class => GuzzleHttp\Client::class,
        ],
        'factories' => [
            Application::class => ApplicationFactory::class,
            Helper\UrlHelper::class => Helper\UrlHelperFactory::class,
            Doctrine\Common\Cache\Cache::class => Factory\DoctrineCacheFactory::class,
            Action\IndexAction::class => Factory\IndexActionFactory::class,
            Action\AddContactAction::class => Factory\AddContactActionFactory::class,
            Action\UpdateContactAction::class => Factory\UpdateContactActionFactory::class,
            Action\RemoveContactAction::class => Factory\RemoveContactActionFactory::class,
        ],
    ],
    'application' => [
        'cache_path' => 'data/cache/',
        'results_per_page' => 10,
        'api' => [
            'base_url' => 'api/',
        ],
        'backend' => [
            'add_contact_endpoint' => array( // uri of the endpoint in the admin panel to process contact details
                'url' => '',
                'parameters' => array(),
            ),
            'update_contact_endpoint' => array( // uri of the endpoint in the admin panel to process contact details
                'url' => '',
                'parameters' => array(),
            ),
            'remove_contact_endpoint' => array( // uri of the endpoint in the admin panel to process contact details
                'url' => '',
                'parameters' => array(),
            ),
            'list_contact_endpoint' => array( // uri of the endpoint in the admin panel to process contact details
                'url' => '',
                'parameters' => array(),
            ),
        ]
    ],
];

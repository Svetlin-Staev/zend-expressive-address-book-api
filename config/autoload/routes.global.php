<?php

use Worldstores\ContactBook\Api\Action;

return [
    'routes' => [
        [
            'name' => 'api.contacts.list',
            'path' => '/index[/{page:\d+}]',
            'middleware' => Action\IndexAction::class,
            'allowed_methods' => ['GET'],
        ],
        [
            'name' => 'api.contacts.add',
            'path' => '/add',
            'middleware' => Action\AddContactAction::class,
            'allowed_methods' => ['POST'],
        ],
        [
            'name' => 'api.contacts.update',
            'path' => '/update',
            'middleware' => Action\UpdateContactAction::class,
            'allowed_methods' => ['PUT'],
        ],
        [
            'name' => 'api.contacts.remove',
            'path' => '/remove/{id:\d+}',
            'middleware' => Action\RemoveContactAction::class,
            'allowed_methods' => ['GET'],
        ],
    ],
];

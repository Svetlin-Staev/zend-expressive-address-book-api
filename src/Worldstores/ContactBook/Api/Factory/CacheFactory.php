<?php

namespace Worldstores\ContactBook\Api\Factory;

use Doctrine\Common\Cache\Cache;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Interop\Container\Exception\NotFoundException;
use Worldstores\ContactBook\Api\Middleware\CacheMiddleware;

class CacheFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @throws NotFoundException
     * @throws ContainerException
     *
     * @return CacheMiddleware
     */
    public function __invoke(ContainerInterface $container)
    {
        $cache = $container->get(Cache::class);

        return new CacheMiddleware($cache);
    }
}
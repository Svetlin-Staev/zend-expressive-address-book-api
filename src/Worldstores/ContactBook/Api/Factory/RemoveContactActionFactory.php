<?php

namespace Worldstores\ContactBook\Api\Factory;

use GuzzleHttp\ClientInterface;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Interop\Container\Exception\NotFoundException;
use Worldstores\ContactBook\Api\Action\AddContactAction;
use Zend\Expressive\Container\Exception;

class RemoveContactActionFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @throws NotFoundException
     * @throws ContainerException
     *
     * @return AddContactAction
     */
    public function __invoke(ContainerInterface $container)
    {
        $client = $container->get(ClientInterface::class);
        $config = $container->get('config');

        if (!array_key_exists('backend', $config['application'])) {
            throw new Exception\NotFoundException('backend must be set in application configuration');
        }

        if (!array_key_exists('remove_contact_endpoint', $config['application']['backend'])) {
            throw new Exception\NotFoundException('contact_endpoint must be set in backend configuration node');
        }

        return new AddContactAction($client, $config['application']['backend']['remove_contact_endpoint']);
    }
}
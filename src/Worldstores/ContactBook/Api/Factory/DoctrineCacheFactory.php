<?php

namespace Worldstores\ContactBook\Api\Factory;

use Doctrine\Common\Cache\FilesystemCache;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Interop\Container\Exception\NotFoundException;
use Zend\Expressive\Container\Exception;

class DoctrineCacheFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @throws NotFoundException
     * @throws ContainerException
     *
     * @throws \InvalidArgumentException
     *
     * @throws Exception\NotFoundException
     *
     * @return FilesystemCache
     */
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');

        if (!array_key_exists('cache_path', $config['application'])) {
            throw new Exception\NotFoundException('cache_path must be set in application configuration');
        }

        return new FilesystemCache($config['application']['cache_path']);
    }
}
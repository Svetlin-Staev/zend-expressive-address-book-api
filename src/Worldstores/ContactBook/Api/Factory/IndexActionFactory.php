<?php

namespace Worldstores\ContactBook\Api\Factory;

use GuzzleHttp\ClientInterface;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Interop\Container\Exception\NotFoundException;
use Worldstores\ContactBook\Api\Action\IndexAction;
use Zend\Expressive\Container\Exception;

class IndexActionFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @throws NotFoundException
     * @throws ContainerException
     *
     * @return IndexAction
     */
    public function __invoke(ContainerInterface $container)
    {
        $client = $container->get(ClientInterface::class);
        $config = $container->get('config');

        if (!array_key_exists('backend', $config['application'])) {
            throw new Exception\NotFoundException('backend must be set in application configuration');
        }

        if (!array_key_exists('list_contact_endpoint', $config['application']['backend'])) {
            throw new Exception\NotFoundException('contact_endpoint must be set in backend configuration node');
        }

        if (!array_key_exists('results_per_page', $config['application'])) {
            throw new Exception\NotFoundException('results_per_page must be set in application configuration');
        }

        $resultsPerPage = $config['application']['results_per_page'];
        $endpoint       = $config['application']['backend']['list_contact_endpoint']['url'];

        return new IndexAction($client, $resultsPerPage, $endpoint);
    }
}
<?php

namespace Worldstores\ContactBook\Api\Action;

use GuzzleHttp;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class AddContactAction
{
    /**
     * @var GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $backendEndpoint;

    /**
     * @param GuzzleHttp\Client $client
     * @param string            $backendEndpoint
     */
    public function __construct(GuzzleHttp\Client $client, $backendEndpoint)
    {
        $this->client = $client;
        $this->backendEndpoint = $backendEndpoint;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable|null $next
     *
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $result = $this->client->post($this->backendEndpoint, $request->getParsedBody())->getBody()->getContents();

        if ($response->getBody()->isWritable()) {
            $response->getBody()->write($result);
        }

        if (null !== $next) {
            return $next($request, $response);
        }

        return $response;
    }
}
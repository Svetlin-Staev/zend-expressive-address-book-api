<?php

namespace Worldstores\ContactBook\Api\Action;

use GuzzleHttp;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Exception;
use Pagerfanta\Pagerfanta;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class IndexAction
{
    /**
     * @var int
     */
    protected $resultsPerPage;

    /**
     * @var GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $backendEndpoint;

    /**
     * @param GuzzleHttp\Client $client
     * @param string            $resultsPerPage
     * @param string            $backendEndpoint
     */
    public function __construct(GuzzleHttp\Client $client, $resultsPerPage, $backendEndpoint)
    {
        $this->client = $client;
        $this->resultsPerPage = $resultsPerPage;
        $this->backendEndpoint = $backendEndpoint;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable|null $next
     *
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     *
     * @throws Exception\NotIntegerMaxPerPageException
     * @throws Exception\NotIntegerCurrentPageException
     * @throws Exception\LessThan1CurrentPageException
     * @throws Exception\LessThan1MaxPerPageException
     * @throws Exception\OutOfRangeCurrentPageException
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $contacts = $this->client->post($this->backendEndpoint, $request->getParsedBody())->getBody()->getContents();

        $pager = new Pagerfanta(new ArrayAdapter($contacts));

        $pager->setMaxPerPage($this->resultsPerPage)->setCurrentPage($request->getAttribute('page', 0));

        if ($response->getBody()->isWritable()) {
            $response->getBody()->write($pager->getIterator()->getArrayCopy());
        }

        if (null !== $next) {
            return $next($request, $response);
        }

        return $response;
    }
}